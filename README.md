# The Moringa Land Cover Toolbox
## Installation via Docker

### Install Docker
*Windows 10+*  
```
Install Docker Desktop then use the command prompt (cmd.exe) to execute docker pull and docker create.
```

*Debian and Ubuntu*  
See full documentation here : https://docs.docker.com/engine/install/ubuntu/

### Use docker to pull, create, exec...
```bash
# Pull image (default tag is "latest")
docker pull gitlab-registry.irstea.fr/raffaele.gaetano/moringav2

# Create a persistent, interactive (named "moringa") container with 
# a volume attached, here with home dir, but it can be any directory. 
# Beware of ownership issues, see the last section of this doc.
docker create --interactive --tty --volume /home/$USER:/home/ubuntu/data \
    --name moringa gitlab-registry.irstea.fr/raffaele.gaetano/moringav2 /bin/bash

# Start the container in interactive mode (brings up a bash shell)
docker start -i moringa

# Start the container in background mode
docker start moringa

# Execute a command in the container while in background mode
docker exec moringa ls -alh

# Stop a container (do this always when you're done!)
docker stop moringa

# Remove a persistent container
docker rm moringa
```

### Fix volume ownership issue (required if host's UID > 1000)
When mounting a volume, you may experience errors while trying to write files from within the container.  
Since the default user (ubuntu) is UID 1000, you won't be able to write files into your volume  
which is mounted with the same UID than your linux host user (may be UID 1001 or more).  
In order to address this, you need to edit the container's user UID and GID to match the right numerical value.
This will only persist in a named container, it is required every time you're creating a new one.

```bash
# Create a named container (here with your HOME as volume), 
# Docker will automatically pull the image.
docker create --interactive --tty --volume /home/$USER:/home/ubuntu/data \
    --name moringa gitlab-registry.irstea.fr/raffaele.gaetano/moringav2 /bin/bash
    
# Start a background container process (in order to exec root commands, 
# because default user isn't sudoer)
docker start moringa

# Exec required commands with user root (here with host's ID, 
# replace $UID and $GID with desired values)
docker exec --user root moringa usermod ubuntu -u $UID
docker exec --user root moringa groupmod ubuntu -g $GID

# Force reset ownership with updated UID and GID. Make sure to double check that.
docker exec moringa id

# Recursive chown will apply to your volume in /home/ubuntu/data
# ATTENTION: ownership is affected also outside the docker. 
# Beware of folders shared with other users.
docker exec --user root moringa chown -R ubuntu:ubuntu /home/ubuntu

# Stop the background container and start a new interactive shell, you're done!
docker stop moringa
docker start -i moringa
```
