from Common.geotools import get_query_bbox
from pystac_client import Client
import planetary_computer as PC
import tqdm
import rasterio
from pyproj import Transformer as T
from shapely.geometry import Polygon
import rasterio.mask
import os

def fetch(shp, dt, out_fld, band_list=None):

    bbox, i_bbox, shp_srs = get_query_bbox(shp, return_all=True)
    api = Client.open('https://planetarycomputer.microsoft.com/api/stac/v1', modifier=PC.sign_inplace)
    res = api.search(collections="landsat-c2-l2", bbox=bbox, datetime=dt)
    lst = ['blue', 'green', 'red', 'nir08', 'swir16', 'swir22', 'qa_pixel']
    if band_list is not None:
        lst = band_list

    prg = tqdm.tqdm(total=len(res.item_collection()) * len(lst), desc="Fetching from Planetary")
    for item in res.items():
        with rasterio.open(item.assets['red'].href) as ds:
            img_srs = ds.crs.to_epsg()
        if shp_srs == img_srs:
            o_bbox = i_bbox
        else:
            tr = T.from_crs(shp_srs, img_srs, always_xy=True)
            x_min, y_min = tr.transform(i_bbox[0], i_bbox[1])
            x_max, y_max = tr.transform(i_bbox[2], i_bbox[3])
            o_bbox = [x_min, y_min, x_max, y_max]

        alst = lst.copy()
        if 'coastal' in item.assets.keys():
            alst.append('coastal')

        for a in alst:
            ofn = os.path.join(out_fld, '/'.join(item.assets[a].get_absolute_href().split('?')[0].split('/')[10:]))
            os.makedirs(os.path.dirname(ofn), exist_ok=True)
            with rasterio.open(item.assets[a].href) as img:
                out_img, out_geot = rasterio.mask.mask(img,
                                                       [Polygon.from_bounds(o_bbox[0], o_bbox[1],
                                                                            o_bbox[2], o_bbox[3])],
                                                       crop=True,
                                                       all_touched=True)
                out_meta = img.meta
            out_meta.update({"driver": "GTiff",
                             "height": out_img.shape[1],
                             "width": out_img.shape[2],
                             "transform": out_geot})
            with rasterio.open(ofn, "w", **out_meta) as dest:
                dest.write(out_img)
            prg.update()
    prg.close()