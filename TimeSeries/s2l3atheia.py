from TimeSeries.s2theia import *

class S2L3ATheiaTilePipeline(S2TheiaTilePipeline):
    # --- BEGIN SENSOR PROTOTYPE ---

    NAME = 'S2-L3A-THEIA'
    PTRN_10m = ['*_FRC_B2.tif', '*_FRC_B3.tif', '*_FRC_B4.tif', '*_FRC_B8.tif']
    PTRN_20m = ['*_FRC_B5.tif', '*_FRC_B6.tif', '*_FRC_B7.tif', '*_FRC_B8A.tif', '*_FRC_B11.tif', '*_FRC_B12.tif']
    PTRN_msk = ['MASKS/*_WGT_R1.tif', 'MASKS/*_FLG_R1.tif', 'MASKS/*_DTS_R1.tif']  #to adjust
    MERG_msk = ['min', 'min', 'min'] #not needed..
    PTRN_ful = PTRN_10m[0:3] + PTRN_20m[0:3] + [PTRN_10m[3]] + PTRN_20m[3:]

class S2L3ATheiaPipeline(S2TheiaPipeline):
    S2TilePipeline = S2L3ATheiaTilePipeline
    _check = S2TilePipeline._check
    _tile_id = S2TilePipeline._tile_id
    # have to adjust if no gapfilling