import os.path
import OBIA.segmentation
import VHR.vhrbase
from TimeSeries import s2theia, s2planetary, s1base, s1planetary, planet_mosaics
from Common import demtools

def run_segmentation(img, threshold, cw, sw , out_seg,
                     n_first_iter, margin, roi, n_proc, memory=None,
                     remove_graph=True, force_parallel=False, light=False):
    if not os.path.exists(os.path.dirname(out_seg)):
        os.makedirs(os.path.dirname(out_seg))
    params = OBIA.segmentation.LSGRMParams(threshold, cw, sw, n_first_iter, margin)
    if light:
        OBIA.segmentation.lsgrm_light(img, params, out_seg, n_proc, memory, roi, force_parallel)
    else:
        OBIA.segmentation.lsgrm(img, params, out_seg, n_proc, memory, roi, remove_graph, force_parallel)
    return out_seg

def preprocess_spot67(in_fld, out_fld, dem_fld, geoid_file, skip_ps, compress,
                      clip, align_to=None, align_to_band=3, align_using_band=1):
    sp = VHR.vhrbase.SPOT67RasterPipeline(in_fld)
    sp.to_toa()
    sp.orthorectify(dem_fld, geoid_file)
    if clip is not None and os.path.exists(clip):
        sp.clip(clip)
    if not skip_ps:
        sp.write_outputs(out_fld, update_pipe=True, compress=compress)
        sp.pansharp()
    if align_to is not None and os.path.exists(align_to):
        sp.rigid_align(align_to, this_band=align_using_band-1, ref_band=align_to_band-1)
    return sp.write_outputs(out_fld, compress=compress)

def preprocess_s2(in_fld, out_fld, output_dates_file=None, roi=None,
                  align_to=None, align_to_band=3, align_using_band=3, provider='theia'):
    S2Processor = None
    if provider == 'theia':
        S2Processor = s2theia.S2TheiaPipeline
    elif provider == 'planetary':
        S2Processor = s2planetary.S2PlaneteryPipeline
    else:
        raise ValueError("Unsupported/non-valid provider")

    s2 = S2Processor(in_fld, temp_fld=out_fld)
    if roi is not None:
        s2.set_roi(roi)
    if output_dates_file is not None:
        s2.set_output_dates_by_file(output_dates_file)
    else:
        raise ValueError("Please provide path to a text file containing output dates.")

    align = align_to is not None
    if (align_to == 'self'):
        align_to = None

    return s2.extract_feature_set(out_fld, store_gapfill=True, mosaicking='vrt', align=align,
                           align_to=align_to, align_to_band=align_to_band, align_using_band=align_using_band)

def preprocess_planet(in_fld, out_fld):
    pl = planet_mosaics.PlanetMosaicPipeline(in_fld)
    pl.compute_features()
    return pl.write_outputs(out_fld)

def preprocess_s1(in_fld, roi, out_fld, dem_fld=None, geoid=None, direction=None, satellite=None,
                  skip_despeckle=False, provider='native'):
    S1processor = None
    if provider == 'native':
        S1Processor = s1base.S1GRDPipeline
    elif provider == 'planetary':
        S1Processor = s1planetary.S1RTCPipeline
    else:
        raise ValueError("Unsupported/non-valid provider")

    s1 = S1Processor(in_fld, roi, out_fld, direction=direction, satellite=satellite)
    if provider == 'native':
        assert(os.path.exists(dem_fld) and os.path.exists(geoid))
        s1.calibrate()
        s1.orthorectify(dem_fld, geoid)
    s1.stitch()
    if not skip_despeckle:
        s1.multitemp_speckle_filter()
    s1.compute_features()
    return s1.write_outputs(out_fld)

def fetch(imagery, shp, out_fld, dt=None, auth=None):
    assert(imagery in ['s2theia', 's2planetary', 's1grd', 's1rtc', 'planetmosaics', 'cop-dem-glo-30', 'nasadem'])
    if imagery not in ['s2planetary', 'cop-dem-glo-30', 'nasadem'] and auth is None:
        raise ValueError("Please provide authentication information.")
    if imagery not in ['cop-dem-glo-30', 'nasadem'] and dt is None:
        raise ValueError("Please provide date range option.")
    if imagery == 's2theia':
        s2theia.fetch(shp, dt, out_fld,auth)
    elif imagery == 's2planetary':
        s2planetary.fetch(shp, dt, out_fld)
    elif imagery == 's1grd':
        s1base.fetch(shp, dt, out_fld, auth)
    elif imagery == 's1rtc':
        s1planetary.fetch(shp, dt, out_fld, auth)
    elif imagery == 'planetmosaics':
        planet_mosaics.fetch(shp, dt, out_fld, auth)
    elif imagery in ['cop-dem-glo-30', 'nasadem']:
        demtools.fetch(shp, out_fld, product=imagery)
    return
